require 'digest'
require 'json'

class BasicPassword
  attr_reader :salt, :hash

  def initialize(value, salt = SecureRandom.base64)
    @salt = salt
    @hash = Digest::SHA2.new(512).base64digest(value + salt)
  end

  def to_h
    { salt: @salt, hash: @hash }
  end

  def to_json
    JSON.generate to_h
  end

  def check(value)
    hash == BasicPassword.new(value, salt).hash
  end

  def self.digest(value, salt = SecureRandom.base64)
    BasicPassword.new(value, salt).to_h
  end

  def self.check (value, salt, hash)
    hash == BasicPassword.new(value, salt).hash
  end
end