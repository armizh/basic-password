Gem::Specification.new do |s|
  s.name      = 'basic-password'
  s.summary   = 'Password generator'
  s.version   = '0.1.1'
  s.license   = 'BSD-2-Clause'
  s.authors   = ['Felipe Cabrera']
  s.email     = 'fecabrera0@outlook.com'
  s.files     = `git ls-files lib`.split(/\n/)
  s.homepage  = 'https://gitlab.com/armizh/basic-password'

  s.add_dependency 'json'
end