# Basic Password
Password generator that uses SHA512.

## Installation
```
gem install secure-password
```

Gemfile:
```
gem 'secure-password'
```